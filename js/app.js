// Initialize Firebase
var config = {
    apiKey: "AIzaSyCChOLSSAa1iQt8RelWPnM0__ykQ_d9tA8",
    authDomain: "demofirebase-5063c.firebaseapp.com",
    databaseURL: "https://demofirebase-5063c.firebaseio.com",
    projectId: "demofirebase-5063c",
    storageBucket: "demofirebase-5063c.appspot.com",
    messagingSenderId: "1052498850416"
};
firebase.initializeApp(config);

// Get a reference to the database service
var database = firebase.database();

var activitiesRef = firebase.database().ref('activities');


/* CREATE */
function create() {
    var data = document.getElementById('input-data').value;

    activitiesRef.push({
        activity: data
    })
}

/* UPDATE */
function update(key) {
    var data = document.getElementById(key).value
    firebase.database().ref('activities/' + key).set({
        activity: data  
    });
}

/* DELETE */
function remove() {
    var key = document.getElementById("delete-select").value
    firebase.database().ref('activities/' + key).remove();
}


/* READ */
activitiesRef.on('value', function (snapshot) {

    var updateSection = '';
    var readSection = '';
    var deleteSection = '<option selected></option>';

    snapshot.forEach(function (activity) {

        var key = activity.key;
        var data = activity.val();


        updateSection +=
            "<div class='row mb-3'>" +
            "<input type='text'class='form-control col-md-9' id='" + key + "' value='" + data.activity + "'>" +
            "<button type='button' class='btn btn-outline-secondary col-md-2 ml-3' onclick= 'update(\"" + key + "\")'>Update</button>" +
            "</div>"

        readSection += "<tr> <td>" + key + "</td> <td>" + data.activity + "</td> </tr>"

        deleteSection += "<option value = '" + key + "'>" + data.activity + "</option>"
    });

    document.getElementById('update-section').innerHTML = updateSection;
    document.getElementById('read-section').innerHTML = readSection;
    document.getElementById('delete-select').innerHTML = deleteSection;
});

